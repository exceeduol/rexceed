% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/core.R
\name{core.is_available}
\alias{core.is_available}
\title{Utility function which returns a flag indicating whether the databases are available}
\usage{
core.is_available(config)
}
\arguments{
\item{config}{rexceed config object (see new_config)}
}
\value{
true/false
}
\description{
Utility function which returns a flag indicating whether the databases are available
}
\examples{
available<-rexceed::core.is_available(config)
}

