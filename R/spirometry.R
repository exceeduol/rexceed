#' Spirometry database constant passed into utility DB functions
#' @export
#' @examples
#' db_rows<-rexceed::core.get_all_db_rows(config,rexceed::spiro.DB,paste0("select read_code from read_version2 where read_code = binary 'H3...'"));
spiro.DB<-"exceed_spirometry";

#' Get spirometry test results (e.g. FEV1, FVC) for a patient identified using an EXCEED ID
#' Use the 'spiro.get_result_timecourse' function to get plottable time-course data.
#' @param config rexceed config object (see new_config)
#' @param exceed_id EXCEED ID
#' @return data.frame containing spirometry data results
#' @export
#' @examples
#' config<-rexceed::core.new_config("rob","pa55word","20160103")
#'
#' #return all spirometry tests done for patient 000037
#' spiro_data<-rexceed::spiro.get_patient_results(config,"000037")
#
spiro.get_patient_results<-function(config,exceed_id) {
  select<-"select id as test_result_id, intVC as vc,
intFEV75 as fev75,
  intFEV1 as fev1,
  intFEV3 as fev3,
  intFEV6 as fev6,
  intFVC as fvc,
  intPEF as pef,
  intMEF75 as mef75,
  intMEF50 as mef50,
  intMEF25 as mef25,
  intMMEF as mmef,
  intEVC as evc_count,
  intVTLcount as vtl_count,
  intFVLxscale as fvl_xscale,
  intFVLyscale as fvl_yscale,
  intVTLxscale as vtl_xscale,
  intVTLyscale as vtl_yscale";
  from<-"from tbl_testresult"
  where<-paste0("where vcEndID like '%-",exceed_id,"-%'");
  rexceed::core.get_all_db_rows(config,rexceed::spiro.DB,paste(select,from,where,sep=" "))
}

#' Get spirometry timecourse data for a specified test result ID
#' Use the 'get_patient_spirometry_results' function to get test result IDs for EXCEED IDs
#' @param config rexceed config object (see new_config)
#' @param exceed_id EXCEED ID
#' @return matrix of paired values containing spirometry timecourse data
#' @export
#' @examples
#' config<-rexceed::core.new_config("rob","pa55word","20160103")
#'
#' #return all spirometry tests done for test result 12341
#' spiro_vtl<-rexceed::spiro.get_result_timecourse(config,12341,data_type="VTL")
#' plot(spiro_vtl)
#
spiro.get_result_timecourse<-function(config,test_result_id,data_type="FVL") {
  select<-paste0("select vc",data_type," as timecourse");
  from<-"from tbl_testresult"
  where<-paste0("where id = '",test_result_id,"'");

  fvlData<-rexceed::core.get_all_db_rows(config,rexceed::spiro.DB,paste(select,from,where,sep=" "))
  if(nrow(fvlData)==0) {
    return(NA)
  }
  fvlTc<-fvlData[1]$timecourse
  fvlRows<-strsplit(fvlTc,'|',fixed=T)
  fvlCells<-lapply(fvlRows,function(x) strsplit(x,',',fixed=T))
  fvlMatrix<-matrix(as.integer(unlist(fvlCells)),ncol=2,byrow=TRUE)
  fvlMatrix
}
