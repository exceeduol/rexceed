#' Primary care database constant passed into utility DB functions
#' @export
#' @examples
#' db_rows<-rexceed::primary.get_all_db_rows(config,rexceed::primary.DB,paste0("select read_code from read_version2 where read_code = binary 'H3...'"));
primary.DB<-"exceed_primarycare";

#' Get patients which have one or more of the provided read codes
#'
#' @param config rexceed config object (see new_config)
#' @param read_codes Vector of READ codes
#' @param read_version can be 2 or 3
#' @return data.frame containing patients which have one or more of the provided read codes
#' @export
#' @examples
#' config<-rexceed::core.new_config("rob","pa55word","20160103")
#' h3_codes_with_pattern<-rexceed::primary.get_codes_matching_pattern(config,"H3...%")
#
primary.get_patients_with_codes<-function(config,read_codes,read_version="3") {
  code_params<-paste0("'",paste(read_codes,collapse="','"),"'")
  sql<-paste0("select pp.exceed_id,p.id,p.gender,p.pseudo_dob,p.deceased,p.date_deceased,
              (select count(*) from clinical_data cd where cd.patient_practice_id = pp.id and
              binary cd.read_code in (",code_params,") and
              cd.read_version='9999R",read_version,"') as clin_data_count
              from patient_practice pp
              left join patient p on pp.patient_id = p.id
              where (select count(*) from clinical_data cd where cd.patient_practice_id = pp.id and binary cd.read_code in (",
              code_params,")
              and cd.read_version='9999R",read_version,"')>0");
  rexceed::core.get_all_db_rows(config,rexceed::primary.DB,sql)
}

#' Get all clinical data records for a single exceed ID
#'
#' @param config rexceed config object (see new_config)
#' @param exceed_id EXCEED ID (can be obtained from database - see get_patients_with_codes)
#' @param read_codes List of READ codes to retrieve for patient
#' @param lookup_terms when TRUE will also return the human readable terms matching the READ codes
#' @return data.frame containing clinical data rows
#' @export
#' @examples
#' config<-rexceed::core.new_config("rob","pa55word","20160103")
#'
#' #return all clinical data for patient practice ID 1222 including term lookups
#' h3_codes_with_pattern<-rexceed::primary.get_patient_clinical_data(config,1222,TRUE)
#
primary.get_patient_clinical_data<-function(config,exceed_id,read_codes,lookup_terms=FALSE,read_version="3") {
  select<-"select cd.data_type,cd.read_code,cd.date_recorded,cd.value1,cd.value2,cd.episode,cd.date_extracted,cd.extract_source";
  from<-"from clinical_data cd left join patient_practice pp on pp.id = cd.patient_practice_id"
  where<-paste0("where cd.read_version='9999R",read_version,"' and pp.exceed_id = '",exceed_id,"'");
  if(length(read_codes)>0) {
    read_clauses<-lapply(read_codes,function(x) paste0("cd.read_code='",x,"'"))
    where<-paste0(where," AND (",paste(read_clauses,collapse=" OR "),")")
  }
  if(lookup_terms==FALSE) {
    clinical_data<-rexceed::core.get_all_db_rows(config,rexceed::primary.DB,paste(select,from,where,sep=" "))
    return(clinical_data);
  }
  select<-paste0(select,", term_30 as term")
  from<-paste0(from," left join read_version",read_version," rt on rt.read_code = binary cd.read_code")

  rexceed::core.get_all_db_rows(config,rexceed::primary.DB, paste(select,from,where,sep=" "))
}

#' Find read codes in the read code tables using terms
#'
#' @param config rexceed config object (see new_config)
#' @param terms List of terms to search for
#' @param and_logic Use 'AND' rather than 'OR' to search for terms (e.g. asthma AND lung function)
#' @param read_version can be 2 or 3
#' @param codes_only Return only READ codes
#' @return data.frame containing terms and their corresponding READ codes/or list of READ codes
#' @export
#' @examples
#' config<-rexceed::core.new_config("rob","pa55word","20160103")
#'
#' #return all read codes containing 'asthma' and 'lung'
#' asthma_lung_codes<-rexceed::primary.find_read_codes(config,c("asthma","lung"),FALSE)
#
primary.find_read_codes<-function(config,terms,use_and=FALSE, read_version="3", codes_only=F) {
  if(use_and==TRUE) {
    logic=" AND ";
  }
  else {
    logic=" OR ";
  }
  select<-"select read_code, coalesce(term_198,term_60,term_30) as term";
  from<-paste0("from read_version",read_version)
  clauses<-lapply(terms,function(x) paste0("coalesce(term_198,term_60,term_30) like '%",x,"%'"))
  where<-paste0("where ",paste(clauses,collapse=logic));

  output<-rexceed::core.get_all_db_rows(config,rexceed::primary.DB, paste(select,from,where,sep=" "))
  if(codes_only==T) {
    return(output$read_code)
  }
  output
}

#' Find terms in the read code tables using read codes
#'
#' @param config rexceed config object (see new_config)
#' @param terms List of READ codes to search for
#' @param read_version can be 2 or 3
#' @return data.frame containing READ codes and their corresponding terms/or list of READ codes
#' @export
#' @examples
#' config<-rexceed::core.new_config("rob","pa55word","20160103")
#'
#' #return all read codes containing 'asthma' and 'lung'
#' asthma_lung_codes<-rexceed::primary.find_read_codes(config,c("d316."),FALSE)
#
primary.find_terms<-function(config,read_codes, read_version="3") {
  logic=" OR ";

  select<-"select read_code, coalesce(term_198,term_60,term_30) as term";
  from<-paste0("from read_version",read_version)
  clauses<-lapply(read_codes,function(x) paste0("read_code = binary '",x,"'"))
  where<-paste0("where ",paste(clauses,collapse=logic));

  output<-rexceed::core.get_all_db_rows(config,rexceed::primary.DB, paste(select,from,where,sep=" "))
  output
}
